from werkzeug.contrib.profiler import ProfilerMiddleware
from app import flask_app


flask_app.config['PROFILE'] = True
flask_app.wsgi_app = ProfilerMiddleware(flask_app.wsgi_app,
                                        restrictions=[30])
flask_app.run(debug=True)
