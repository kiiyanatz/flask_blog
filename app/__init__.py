from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_mail import Mail
from flask_babel import Babel
from .momentjs import momentjs
from flask_debugtoolbar import DebugToolbarExtension
from config import basedir, ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, \
    MAIL_PASSWORD


flask_app = Flask(__name__)
flask_app.config.from_object('config')
flask_app.jinja_env.globals['momentjs'] = momentjs
db = SQLAlchemy(flask_app)
lm = LoginManager(flask_app)
bcrypt = Bcrypt(flask_app)
mail = Mail(flask_app)
babel = Babel(flask_app)
toolbar = DebugToolbarExtension(flask_app)

# TODO Impliment email error logging

if not flask_app.debug:
    import logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('tmp/microblog.log', 'a',
                                       1 * 1024 * 1024,
                                       10
                                       )
    file_handler.setFormatter
    (
        logging.Formatter
        (
            '%(asctime)s%(levelname)s:%(message)s [in %(pathname)s:%(lineno)d]'
        )
    )
    flask_app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    flask_app.logger.addHandler(file_handler)
    flask_app.logger.info('microblog startup')

from app import views, models
