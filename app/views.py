from config import POSTS_PER_PAGE, MAX_SEARCH_RESULTS
from app import flask_app, db, lm, bcrypt
from app import models
from datetime import datetime
from sqlalchemy.exc import IntegrityError
from .emails import follower_notification
from werkzeug.utils import secure_filename
from app.forms import LoginForm, SignupForm, EditForm, \
    PostForm, SearchForm
from flask import render_template, flash, redirect, \
    request, url_for, g
from flask_login import login_user, current_user, logout_user, \
    login_required


@flask_app.before_request
def before_request():
    if current_user.is_authenticated:
        g.search_form = SearchForm()


@flask_app.route('/', methods=['POST', 'GET'])
@flask_app.route('/<int:page>', methods=['GET', 'POST'])
def index(page=1):
    form = PostForm()
    user = current_user
    if user.is_authenticated:
        if form.validate_on_submit():
            filename = secure_filename(form.post_image.data.filename)
            if filename == "":
                post = models.Post(
                    body=form.post.data,
                    timestamp=datetime.utcnow(),
                    author=current_user,
                    image_path=""
                )
            else:
                upload_path = 'app/static/img/uploads/' + filename
                url_path = 'static/img/uploads/' + filename
                form.post_image.data.save(upload_path)
                post = models.Post(
                    body=form.post.data,
                    timestamp=datetime.utcnow(),
                    author=current_user,
                    image_path=url_path
                )
            db.session.add(post)
            db.session.commit()
            flash('Your post is now live!')
            return redirect(url_for('index'))
        posts = current_user.followed_posts().paginate(page,
                                                       POSTS_PER_PAGE,
                                                       False)

        return render_template('index.html',
                               title='Home',
                               user=user,
                               posts=posts,
                               form=form
                               )
    else:
        return redirect(url_for('login'))


@lm.user_loader
def load_user(id):
    return models.User.query.get(int(id))


@flask_app.route('/del_post/<post_id>', methods=['POST', 'GET'])
@login_required
def del_post(post_id):
    current_user.delete_post(post_id)
    flash("Your post has been deleted")
    return redirect(url_for('index'))


@flask_app.route('/edit_post/<post_id>', methods=['POST'])
@login_required
def edit_post(post_id):
    pass


@flask_app.route('/search', methods=['GET', 'POST'])
@login_required
def search():
    form = SearchForm()
    print form.query.data
    if not form.validate_on_submit():
        print "Error occured"
        return redirect(url_for('index'))
    return redirect(url_for('search_results', query=form.query.data))


@flask_app.route('/search_results/<query>')
@login_required
def search_results(query):
    results = models.Post.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
    return render_template('search_results.html',
                           query=query,
                           results=results)


@flask_app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        user = models.User.query.filter_by(
            username=request.form['username']).first()
        if user is None:
            flash('Account does not exist')
            return redirect(url_for('login'))
        if bcrypt.check_password_hash(user.password,
                                      request.form['password']):
            login_user(user)
            user.last_seen = datetime.utcnow()
            db.session.add(user)
            db.session.commit()
            flash('Login success')
            return redirect(url_for('index'))
        else:
            flash('Incorrect password')
            return redirect(url_for('login'))

    return render_template('login.html',
                           form=form)


@flask_app.route('/signup', methods=['POST', 'GET'])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        user = models.User(username=request.form['username'],
                           email=request.form['email'],
                           password=bcrypt.generate_password_hash(request.form[
                               'password']))
        # TODO Fix duplicate without using exception handling
        try:
            db.session.add(user)
            db.session.commit()
            db.session.add(user.follow(user))
            db.session.commit()
        except IntegrityError as e:
            flash('Username is taken' + e.message)
            return redirect(url_for('signup'))
        else:
            flash('Hi ' + str(request.form['username']) +
                  ' your account has been created, login to procceed')

        return redirect(url_for('login'))
    return render_template('signup.html', form=form)


@flask_app.route('/logout', methods=['POST', 'GET'])
def logout():
    logout_user()
    return redirect(url_for('login'))


@flask_app.route('/profile/<username>')
@flask_app.route('/profile/<username>/<int:page>')
def profile(username, page=1):
    user = models.User.query.filter_by(username=username).first()
    if current_user.is_authenticated:
        if user is None:
            flash('User %s not found.' % username)
            return redirect(url_for('index'))
        elif current_user is None:
            return redirect(url_for('login'))
        posts = user.posts.paginate(page, POSTS_PER_PAGE, False)
        return render_template('profile.html',
                               user=user,
                               posts=posts
                               )
    elif current_user.is_anonymous:
        flash('Please login to procceed')
        return redirect(url_for('login'))


@flask_app.route('/edit', methods=['POST', 'GET'])
@login_required
def profile_edit():
    form = EditForm(current_user.username)
    if request.method == 'POST':
        if form.validate_on_submit():
            current_user.username = form.username.data
            current_user.about_me = form.about_me.data
            db.session.add(current_user)
            db.session.commit()
            flash("Profile updated.")
            return redirect(url_for('profile_edit'))
        else:
            form.username.data = current_user.username
            form.about_me.data = current_user.about_me
    return render_template('edit.html', form=form)


@flask_app.route('/follow/<username>')
@login_required
def follow(username):
    user = models.User.query.filter_by(username=username).first()
    if user is None:
        flash('User %s not found.' % username)
        return redirect(url_for('index'))
    if user == current_user:
        flash('You can\'t follow yourself!')
        return redirect(url_for('index'))
    u = current_user.follow(user)
    if u is None:
        flash('Cannot follow ' + username + '.')
        return redirect(url_for('profile', username=username))
    db.session.add(u)
    db.session.commit()
    flash('You are now following ' + username + '!')
    follower_notification(user, current_user)
    return redirect(url_for('profile', username=username))


@flask_app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = models.User.query.filter_by(username=username).first()
    if user is None:
        flash('User %s not found.' % username)
        return redirect(url_for('index'))
    if user == current_user:
        flash('You can\'t unfollow yourself!')
        return redirect(url_for('profile', username=username))
    u = current_user.unfollow(user)
    if u is None:
        flash('Cannot unfollow ' + username + '.')
        return redirect(url_for('profile', username=username))
    db.session.add(u)
    db.session.commit()
    flash('You have stopped following ' + username + '.')
    return redirect(url_for('profile', username=username))


@flask_app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@flask_app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500
