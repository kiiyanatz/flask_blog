from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, PasswordField, TextAreaField, \
    FileField
from wtforms.validators import DataRequired, EqualTo
from app.models import User


class SearchForm(FlaskForm):
    query = StringField('Query', validators=[DataRequired()])


class PostForm(FlaskForm):
    post = TextAreaField('post', validators=[DataRequired()])
    post_image = FileField('Image')


class LoginForm(FlaskForm):
    username = StringField(label='Username',
                           validators=[DataRequired()])
    password = PasswordField(label='Password',
                             validators=[DataRequired()])


class SignupForm(FlaskForm):
    username = StringField(label='Username',
                           validators=[DataRequired()])

    email = StringField(label='Email',
                        validators=[DataRequired()])

    password = PasswordField(label='Password',
                             validators=[DataRequired(),
                                         EqualTo('confirm',
                                                 message='Passwords must match'
                                                 )
                                         ]
                             )
    confirm = PasswordField('Repeat Password')
    signup = SubmitField(label='Signup')


class EditForm(FlaskForm):
    username = StringField(label='Username',
                           validators=[DataRequired()])
    about_me = TextAreaField(label='About Me',
                             validators=[DataRequired()])

    def __init__(self, original_username, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.original_username = original_username

    def validate(self):
        if not FlaskForm.validate(self):
            return False
        if self.username.data == self.original_username:
            return True
        user = User.query.filter_by(username=self.username.data).first()
        if user is not None:
            self.username.errors.append('This username is taken')
            return False
        return True
