import os

WTF_CSRF_ENABLED = True
SECRET_KEY = 'thisisaprivatekey'

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# Error report config
MAIL_SERVER = 'smtp.googlemail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = '<GMAIL USERNAME HERE>'
MAIL_PASSWORD = '<GMAIL PASSWORD HERE>'

ADMINS = ['<ADMIN EMAIL HERE>']

# Pagination config
POSTS_PER_PAGE = 6

# Flask Woosh config
WHOOSH_BASE = os.path.join(basedir, 'search.db')
MAX_SEARCH_RESULTS = 50
