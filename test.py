#!flask/bin/python
import os
import unittest
from datetime import datetime, timedelta

from config import basedir
from app import flask_app, db
from app.models import User, Post


class TestCase(unittest.TestCase):

    def setUp(self):
        flask_app.config['TESTING'] = True
        flask_app.config['WTF_CSRF_ENABLED'] = False
        flask_app.config['SQLALCHEMY_DATABASE_URI'] = \
            'sqlite:///' + os.path.join(basedir, 'test.db')
        self.flask_app = flask_app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_avatar(self):
        u = User(username='john', email='john@example.com')
        avatar = u.avatar(128)
        expected = \
            'http://www.gravatar.com/avatar/d4c74594d841139328695756648b6bd6'
        assert avatar[0:len(expected)] == expected

    def test_make_unique_username(self):
        u = User(username='john', email='john@example.com')
        db.session.add(u)
        db.session.commit()
        username = User.make_unique_username('john')
        assert username != 'john'
        u = User(username=username, email='susan@example.com')
        db.session.add(u)
        db.session.commit()
        nickname2 = User.make_unique_username('john')
        assert nickname2 != 'john'
        assert nickname2 != username

    def test_follow(self):
        # Create users ie u1 and u2
        u1 = User(username='kiiyanatz', email='kiiyanatz@gmail.com')
        u2 = User(username='kiiyaerick', email='kiiyaerick@gmail.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()
        assert u1.unfollow(u2) is None
        u = u1.follow(u2)
        db.session.add(u)
        db.session.commit()
        assert u1.follow(u2) is None
        assert u1.is_following(u2)
        assert u1.followed.count() == 1
        assert u1.followed.first().username == 'kiiyaerick'
        assert u2.followers.count() == 1
        assert u2.followers.first().username == 'kiiyanatz'
        u = u1.unfollow(u2)
        assert u is not None
        db.session.add(u)
        db.session.commit()
        assert not u1.is_following(u2)
        assert u1.followed.count() == 0
        assert u2.followers.count() == 0

    def test_follow_posts(self):
        # Create for users
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        u3 = User(username='mary', email='mary@example.com')
        u4 = User(username='david', email='david@example.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.add(u3)
        db.session.add(u4)

        # Make four posts
        utcnow = datetime.utcnow()
        p1 = Post(body="Post from john", author=u1,
                  timestamp=utcnow + timedelta(seconds=1))

        p2 = Post(body="Post from susan", author=u2,
                  timestamp=utcnow + timedelta(seconds=2))

        p3 = Post(body="Post from mary", author=u3,
                  timestamp=utcnow + timedelta(seconds=3))

        p4 = Post(body="Post from david", author=u4,
                  timestamp=utcnow + timedelta(seconds=4))
        db.session.add(p1)
        db.session.add(p2)
        db.session.add(p3)
        db.session.add(p4)

        # Setup followers
        u1.follow(u1)
        u1.follow(u2)
        u1.follow(u4)
        u2.follow(u2)
        u2.follow(u3)
        u3.follow(u3)
        u3.follow(u4)
        u4.follow(u4)
        db.session.add(u1)
        db.session.add(u2)
        db.session.add(u3)
        db.session.add(u4)
        db.session.commit()

        # Check the followed posts of each User
        f1 = u1.followed_posts().all()
        f2 = u2.followed_posts().all()
        f3 = u3.followed_posts().all()
        f4 = u4.followed_posts().all()

        assert len(f1) == 3
        assert len(f2) == 2
        assert len(f3) == 2
        assert len(f4) == 1
        assert f1 == [p4, p2, p1]
        assert f2 == [p3, p2]
        assert f3 == [p4, p3]
        assert f4 == [p4]

    def test_delete_post(self):
        # Create for users
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        u3 = User(username='mary', email='mary@example.com')
        u4 = User(username='david', email='david@example.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.add(u3)
        db.session.add(u4)

        # Make four posts
        utcnow = datetime.utcnow()
        p1 = Post(body="Post from john", author=u1,
                  timestamp=utcnow + timedelta(seconds=1))

        p2 = Post(body="Post from susan", author=u2,
                  timestamp=utcnow + timedelta(seconds=2))

        p3 = Post(body="Post from mary", author=u3,
                  timestamp=utcnow + timedelta(seconds=3))

        p4 = Post(body="Post from david", author=u4,
                  timestamp=utcnow + timedelta(seconds=4))

        p5 = Post(body="Second post from john", author=u1,
                  timestamp=utcnow + timedelta(seconds=5))

        db.session.add(p1)
        db.session.add(p2)
        db.session.add(p3)
        db.session.add(p4)
        db.session.add(p5)
        self.assertTrue(u1.delete_post(5))
        print p4.id

if __name__ == '__main__':
    unittest.main()
